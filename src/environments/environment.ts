// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyAi20U-X9acTGcdhnLY1uUbiQ2_-P-tYuo",
    authDomain: "todolistapp-8fe48.firebaseapp.com",
    databaseURL: "https://todolistapp-8fe48.firebaseio.com",
    projectId: "todolistapp-8fe48",
    storageBucket: "todolistapp-8fe48.appspot.com",
    messagingSenderId: "563493556722"
  }
};
