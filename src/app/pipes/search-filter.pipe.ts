import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchFilter'
})
export class SearchFilterPipe implements PipeTransform {

  transform(toDoListArray: any, searchText: any): any {
    if (toDoListArray) {
      let search;
      (searchText != undefined) ? search = searchText : search = '';
      return toDoListArray.filter(function(item) {
        if (item.title.toUpperCase().includes(search.toUpperCase())){
          return item;
        } else {
          return null;
        }
      });
    } else {
      return null;
    }
  }

}
