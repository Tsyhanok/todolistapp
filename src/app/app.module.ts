import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { TodoComponent } from './todo/todo.component';
import { SearchComponent } from './todo/search/search.component';
import { ListItemComponent } from './todo/list-item/list-item.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { SearchFilterPipe } from './pipes/search-filter.pipe';

const appRoutes: Routes =[
  { path: '', component: TodoComponent},
  { path: 'item/:id', component: ListItemComponent },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    TodoComponent,
    SearchComponent,
    SearchFilterPipe,
    ListItemComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
