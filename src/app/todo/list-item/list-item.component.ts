import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { TodoService } from '../../services/todo/todo.service';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.css'],
  providers : [TodoService]
})

export class ListItemComponent implements OnInit {

  private id: string;
  private title: string;
  private isChacked: boolean;
  private routeSubscription: Subscription;
  private toDoListItem: any;

  constructor(private route: ActivatedRoute, private toDoService: TodoService){
    this.routeSubscription = route.params.subscribe( params => this.id = params['id'] );
  }

  ngOnInit() {
    let list = [];
    this.toDoService.getToDoList().snapshotChanges()
    .subscribe(item => {
      list = [];
      item.forEach(el => {
        let x = el.payload.toJSON();
        x["$key"] = el.key;
        list.push(x);
      })

      this.toDoListItem = list.filter( item => item.$key === this.id );
    });
  }

  alterCheck($key: string, isChecked) {
    this.toDoService.checkOrUnCheckTitle($key,!isChecked);
  }

  updateTitle(newTitle: string) {
    this.toDoService.updateTitle(this.id, newTitle);
  }

}
