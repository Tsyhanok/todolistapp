import { Input, Component, OnInit } from '@angular/core';
import { SearchFilterPipe } from '../../pipes/search-filter.pipe';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  providers: [ SearchFilterPipe ]
})
export class SearchComponent implements OnInit {
  @Input() toDoListArray: any[];
  public isOpen: boolean = false;
  public searchText: string = '';

  constructor() {
  }

  ngOnInit() {
  }

  toogleSearchBox() {
    this.isOpen = !this.isOpen;
  }

}
